############################################################################
FOLDING INPUT RNA
############################################################################

This project has two folders:
- docs:   Contains the problem description (rnaFold.pdf)
- python: Contains two python programs (rna_fold.py, rna_fold_alternate.py)
           and an input file (input.txt)

The program rna_fold.py (python 3.3) finds the maximum valid pairings
for an RNA sequence.  To find the maximum pairings, the program uses 
simulated annealing.

To run the program, type:
>python3 rna_fold.py

To start, the program reads an input file called input.txt that exists in 
the same directory as the program.  The output to this program is a 
single integer of the maximum number of base pairs.

The program is set to let the routine run for 50 seconds before returning
the maximum number of pairings.  Since we are allowed 1 minute, I wanted to 
be conservative to make sure something was returned. Commented out for 
submission, the program contains code to print the length of time taken.


############################################################################
ALTERNATIVE VERSION (rna_fold_alt.py)

I have included as an alternative an implementation that applies a genetic 
algorithm to the problem.  It is not as fast as the above, but I included 
it because I spent quite a bit of time on it and felt like it could be a
good back-up, as well as an illustration of applying genetic algorithms
to the problem.

You can run this alternative version of the program (also python 3.3):
>python3 rna_fold_alternate.py

Similar as above, the program reads an input file called input.txt that 
exists in the same directory as the program.  The output to this program 
is a single integer of the maximum number of base pairs.  Since we are 
allowed 1 minute, I wanted to be conservative to make sure something was 
returned. Commented out for submission, the program contains code to print 
the length of time taken.

As mentioned above, I found my implentation of the genetic algorithm 
slower than simulated annealing.  In tests on my Macbook, for the case 
with 2433 maximum pairings, the program using simulated annealing was 
able to find the solution in ~11.8 seconds, while the genetic algorithm 
found the solution in ~18.2 s.


############################################################################
Katherine Haynes
CS440 Assignment 3
2018/03/13