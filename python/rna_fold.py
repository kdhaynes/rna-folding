#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  9 14:22:17 2019

@author: kdhaynes
"""
# Program to calculate the maximum number of base pairs in an RNA sequence.
# For RNA, possible bases (nucleotides) are A, C, G, U.
# The pairings are A-U, C-G, and G-U.
#
# To get the RNA sequence, an input file called 'input.txt' is read in
# from the current directory.  In input.txt is a single line of characters
# containing the RNA sequence (random order of bases in upper case).
#
# The output to this program is a single integer of the maximum
# number of base pairs.
#
# Commented out for submission, following the maximum pairings, the
# program outputs the length of time taken to find the solution (s).
#
# The program is set to stop the routine in 50 seconds.
#
# To find the maximum pairings, this program uses simulated annealing.
# In finding the nearest neighbor, it preferentially uses either the
# A-U and C-G pairings.

import copy, math, random, time
from collections import OrderedDict

# An RNA molecule is represented as a list of single 
# four capital letters: 'A','C','G','U'.
# Each RNA holds the string of bases and the pairings.
class RNA:
    # Upon initialization, save the sequence, the number of bases, 
    # the possible pairs and the total number of possible pairs.
    # Input: An RNA sequence in a string of capital letters.  
    def __init__(self, bstring):
        self.nbases = len(bstring)
        self.bstring = bstring
        self.npairs, self.allpairs2, self.allpairs1 = \
            self.find_allpairs(bstring)
            
        keys2 = set(self.allpairs2.keys())
        keys1 = set(self.allpairs1.keys())
        self.keylist = list(keys2.union(keys1))       

    # Test to see if two bases can pair.
    # A-U and C-G pairings are preferential, thus are weight 2.
    # G-U pairings are less preferrable, thus are of weight 1.
    # Input: A pair of RNA bases (tuple).
    # Output: Either none (no pairings), or the weight of the
    # pairing (integer).
    def ispair(self, bpair):
        if (bpair in [['A','U'],['U','A'],['C','G'],['G','C']]):
            return 2
        elif (bpair in [['G','U'],['U','G']]):
            return 1
        else:
            return None

    # Determine the score of dictionary of pairs.
    # In this case, it's the number of pairs.
    # Input: An individual (dictionary).
    # Output: Number of pairs (integer).
    def score(self,individual):
        return len(individual)
    
    # Get all possible pairs in the strand, and store
    # them in a dictionary.
    # Input: RNA sequence (string).
    # Output: Total number of possible pairs (integer),
    # all possible preferrential (A-U/C-G) pairs (dictionary),
    # and all possible G-U pairs (dictionary).
    def find_allpairs(self, bstring):
        allpairs2=OrderedDict()
        allpairs1=OrderedDict()
        nlen = len(bstring)
        npairs = 0
        for i in range(nlen):
            for j in range(i+4,nlen):
                myweight = self.ispair([bstring[i],bstring[j]])
                if (myweight == 2):
                   if (i in allpairs2):
                       allpairs2[i].add(j)
                   else:
                       allpairs2[i] = {j}
                   npairs += 1
                if (myweight == 1):
                    if (i in allpairs1):
                        allpairs1[i].add(j)
                    else:
                        allpairs1[i] = {j}
                    npairs += 1
                    
        return npairs, allpairs2, allpairs1

    # Get a single random valid pair combination,
    # preferrentially selecting A-U/C-G pairs.
    # Input: None.
    # Output: A valid pair combination (dictionary).
    def find_individual(self):
         vbases = set()
         vpairs = OrderedDict()
 
         random.shuffle(self.keylist)
         for key in self.keylist:
             if (key not in vbases):
                if (key in self.allpairs2):
                    tlist = list(self.allpairs2[key]-vbases)
                else:
                    tlist = []
                    
                if ((not tlist) and (key in self.allpairs1)):
                    tlist = list(self.allpairs1[key]-vbases)

                if (tlist):
                    pnow = tlist[random.randrange(len(tlist))]
                    vpairs[key] = pnow
                    vbases = vbases | {key,pnow}

         return vpairs
        
    # Routine to find a neighbor, changing one pair.
    # Input: A valid pair combination (dictionary).
    # Output: A new valid pair combination (dictionary).
    def find_neighbor(self, parent):       
        child = copy.deepcopy(parent)
        childkeys = set(child.keys())
        child.pop(list(childkeys)[random.randrange(len(childkeys))])
        childall = set(list(child.keys()) + list(child.values()))

        random.shuffle(self.keylist)
        for key in self.keylist:
            if (key not in childall):
                if (key in self.allpairs2):
                    tlist = list(self.allpairs2[key]-childall)
                else:
                    tlist = []
                    
                if ((not tlist) and (key in self.allpairs1)):
                    tlist = list(self.allpairs1[key]-childall)

                if (tlist):
                    pnow = tlist[random.randrange(len(tlist))]
                    child[key] = pnow
                    childall = childall | {key,pnow}
            
        return child
        

# Simulated Annealing Algorithm.
# Input: An RNA (instance of class RNA), and
# the starting time (real number).
# Output: The maximum number of pairings (integer).
def anneal(oneRNA, t0):

    # Acceptance Probability Function
    # Input: The old score (integer), the new score (integer),
    # and the time (real number).
    # Output: Probability (real number).
    def accept_prob(oldscore, newscore, T):
        alpha = math.exp((newscore-oldscore)/T)
        return alpha
    
    
    # Main Routine
    T = 1.0
    Tmin = 0.0001
    delta = 0.95
    nloops = 100
    niterconst = 100
    
    parent = oneRNA.find_individual()
    nmaxpairs = oneRNA.score(parent)
    if (oneRNA.npairs <= 1):
        return nmaxpairs
    
    niters = 0
    while ((T > Tmin) and (time.time() - t0 < 50) \
            and (niters < niterconst)):
        
        for i in range(nloops):
            child = oneRNA.find_neighbor(parent)
            noldpairs = oneRNA.score(parent)
            nnewpairs = oneRNA.score(child)
            
            if (nmaxpairs < nnewpairs):
                nmaxpairs = nnewpairs
                niters = 0
            else:
                niters += 1

            ap = accept_prob(noldpairs, nnewpairs, T)
            if (random.random() < ap):
                parent = child
                
        T = T*delta

    return nmaxpairs

# Random Weighted Selection Algorithm.
# Input: The elements being selected (list) and
# the corresponding weights (list of same length).
# Output: The selection (element of the list).
def rw_select(mylist, myweights):
    rnd = random.random() * sum(myweights)
    for i, w in enumerate(myweights):
        rnd -= w
        if rnd < 0:
            return mylist[i]

#==============================================================================
  
# Start timer:
t0=time.time()

# Read in input file:
filename='input.txt'
mylist=[]
with open(filename,'r') as f:
    data = f.readline().rstrip()

# Run the simulated annealing algorithm:
myrna = RNA(list(data))
nmaxpairs = anneal(myrna, t0)
print(nmaxpairs)

# Print out the time taken:
#timetaken = time.time() - t0
#print ()
#print ("Time Taken (s): ",'{:.5f}'.format(timetaken))
    

