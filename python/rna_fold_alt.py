#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  9 14:22:17 2019

@author: kdhaynes
"""
# Program to calculate the maximum number of base pairs in an RNA sequence.
# For RNA, possible bases (nucleotides) are A, C, G, U.
# The pairings are A-U, C-G, and G-U.
#
# To get the RNA sequence, an input file called 'input.txt' is read in
# from the current directory.  In input.txt is a single line of characters
# containing the RNA sequence (random order of bases in upper case).
#
# The output to this program is a single integer of the maximum
# number of base pairs.
#
# Commented out for submission, following the maximum pairings, the
# program outputs the length of time taken to find the solution (s).
#
# The program is set to stop the routine in 50 seconds.
#
# To find the maximum pairings, this program uses a genetic algorithm.
# In both the reproduction and mutation, it preferentially uses
# the A-U and C-G pairings.

import random, time
from collections import OrderedDict

# An RNA molecule is represented as a list of single 
# four capital letters: 'A','C','G','U'.
# Each RNA holds the string of bases and the pairings.
class RNA:
    # Upon initialization, save the sequence, the number of bases, 
    # the possible pairs and the total number of possible pairs.
    # Input: An RNA sequence in a string of capital letters.  
    def __init__(self, bstring):
        self.nbases = len(bstring)
        self.bstring = bstring
        self.npairs, self.allpairs2, self.allpairs1 = \
            self.find_allpairs(bstring)
            
        keys2 = set(self.allpairs2.keys())
        keys1 = set(self.allpairs1.keys())
        self.keylist = list(keys2.union(keys1))       

    # Test to see if two bases can pair.
    # A-U and C-G pairings are preferential, thus are weight 2.
    # G-U pairings are less preferrable, thus are of weight 1.
    # Input: A pair of RNA bases (tuple).
    # Output: Either none (no pairings), or the weight of the
    # pairing (integer).
    def ispair(self, bpair):
        if (bpair in [['A','U'],['U','A'],['C','G'],['G','C']]):
            return 2
        elif (bpair in [['G','U'],['U','G']]):
            return 1
        else:
            return None

    # Determine the score of dictionary of pairs.
    # In this case, it's the number of pairs.
    # Input: An individual (dictionary).
    # Output: Number of pairs (integer).
    def score(self,individual):
        return len(individual)

    # Get all possible pairs in the strand, and store
    # them in a dictionary.
    # Input: RNA sequence (string).
    # Output: Total number of possible pairs (integer),
    # all possible preferrential (A-U/C-G) pairs (dictionary),
    # and all possible G-U pairs (dictionary).
    def find_allpairs(self, bstring):
        allpairs2=OrderedDict()
        allpairs1=OrderedDict()
        nlen = len(bstring)
        npairs = 0
        for i in range(nlen):
            for j in range(i+4,nlen):
                myweight = self.ispair([bstring[i],bstring[j]])
                if (myweight == 2):
                   if (i in allpairs2):
                       allpairs2[i].add(j)
                   else:
                       allpairs2[i] = {j}
                   npairs += 1
                if (myweight == 1):
                    if (i in allpairs1):
                        allpairs1[i].add(j)
                    else:
                        allpairs1[i] = {j}
                    npairs += 1
                    
        return npairs, allpairs2, allpairs1

    # Get a single random valid pair combination,
    # preferrentially selecting A-U/C-G pairs.
    # Input: None.
    # Output: A valid pair combination (dictionary).
    def find_individual(self):
         vbases = set()
         vpairs = OrderedDict()
 
         random.shuffle(self.keylist)
         for key in self.keylist:
             if (key not in vbases):
                if (key in self.allpairs2):
                    tlist = list(self.allpairs2[key]-vbases)
                else:
                    tlist = []
                    
                if ((not tlist) and (key in self.allpairs1)):
                    tlist = list(self.allpairs1[key]-vbases)

                if (tlist):
                    pnow = tlist[random.randrange(len(tlist))]
                    vpairs[key] = pnow
                    vbases = vbases | {key,pnow}
         return vpairs

    # Routine to find a neighbor, changing one pair.
    # Input: A valid pair combination (dictionary).
    # Output: A new valid pair combination (dictionary).
    def find_neighbor(self, child):       
        childkeys = set(child.keys())
        child.pop(list(childkeys)[random.randrange(len(childkeys))])
        childall = set(list(child.keys()) + list(child.values()))

        random.shuffle(self.keylist)
        for key in self.keylist:
            if (key not in childall):
                if (key in self.allpairs2):
                    tlist = list(self.allpairs2[key]-childall)
                else:
                    tlist = []
                    
                if ((not tlist) and (key in self.allpairs1)):
                    tlist = list(self.allpairs1[key]-childall)

                if (tlist):
                    pnow = tlist[random.randrange(len(tlist))]
                    child[key] = pnow
                    childall = childall | {key,pnow}
            
        return child
    
    # Create an initial population of valid pair combinations.
    # Input: None.
    # Output: A population (dictionary) and the corresponding
    # scores for the population (dictionary).
    def find_population(self,maxsize=25):
        score = OrderedDict()
        population = OrderedDict()
        for i in range(maxsize):
            individual = self.find_individual()
            population[i] = individual
            score[i] = self.score(individual)
        
        return population, score

# Genetic Algorithm
def genetic_algorithm(oneRNA, population, score, t0):
    # Routine to reproduce, creating a new individual 
    # from two parents randomly.  Preferentially selects
    # A-U and G-C pairs.
    # Input: Parents (i.e, two valid pair combinations, list).
    # Output: A child (i.e. a new valid pair combination, dictionary).
    def reproduce(parents):
        allpairsa = parents[0]
        allpairsb = parents[1]
        keysa = set(allpairsa.keys())
        keysb = set(allpairsb.keys())
        keylist = list(keysa.union(keysb))
        
        vbases = set()
        vpairs = OrderedDict()
 
        random.shuffle(keylist)
        for key in keylist:
            if (key not in vbases):
               if (key in allpairsa):
                   myset = {allpairsa[key]} - vbases
                   if (myset):
                      if (key in oneRNA.allpairs2):
                          myset2 = myset.intersection(oneRNA.allpairs2[key])
                          if (myset2):
                              myset = myset2
                      pnow = list(myset)[random.randrange(len(myset))]
                      vpairs[key] = pnow
                      vbases |= {key,pnow}
   
               else:
                    myset = {allpairsb[key]}-vbases
                    if (myset):
                        pnow = allpairsb[key]
                        vpairs[key] = pnow
                        vbases |= {key,pnow}
        return vpairs
        
    # Main program for genetic algorithm
    ngen = 100
    pmut = 0.40
    niterconst = 3

    nmaxpairs = max(list(initscore.values()))
    if (nmaxpairs < 2):
        return nmaxpairs
        
    # Loop through generations
    niters = 0
    for i in range(ngen):
       
        # Break out of the generational loop when time runs out
        if ((time.time() - t0) > 50):
            return nmaxpairs
        
        # Create a new population
        new_population = OrderedDict()
        new_score = OrderedDict()
        for i in range(len(population)):
            bothparent = rw_select2(population, score)
            child = reproduce(bothparent)
            if random.random() < pmut:
                child = oneRNA.find_neighbor(child)
                
            new_population[i] = child
            new_score[i] = oneRNA.score(child)
        
        population = new_population
        score = new_score
        
        # Each generation, check to see if we are improving
        if (nmaxpairs < max(score.values())):
            nmaxpairs = max(score.values())
            niters = 0
        else:
            niters += 1
            if (niters > niterconst):
                break
            
    return nmaxpairs

# Random Weighted Selection Algorithm 
# Input: The elements being selected (list) and
# the corresponding weights (list of same length).
# Output: The selection (element of the list).
def rw_select(mylist, myweights):
    rnd = random.random() * sum(myweights)
    for i, w in enumerate(myweights):
        rnd -= w
        if rnd < 0:
            return mylist[i]

# Random Weighted Selection Algorithm 
#  for individuals from a population.
# Input: A population (list of dictionaries).
# Output: Two individual selections (list of dictionaries).
def rw_select2(mypopulation, myscore):
    myscores = list(myscore.values())
    selections = []
    for s in range(2):
        rnd = random.random()*sum(myscores)
        for i,w in enumerate(myscores):
            rnd -= w
            if rnd < 0:
               selections.append(mypopulation[i])
               myscores[i] = 0
    return selections

#==============================================================================
  
# Start timer:
t0=time.time()

# Read in input file:
filename='input.txt'
mylist=[]
with open(filename,'r') as f:
    data = f.readline().rstrip()

# Create the RNA:
myrna = RNA(list(data))

# Create the initial population:
initpop, initscore = myrna.find_population()

# Run the genetic algorithm:
nmaxpairs = genetic_algorithm(myrna, initpop, initscore, t0)
print(nmaxpairs)

# Print out the time taken:
#timetaken = time.time() - t0
#print ()
#print ("Time Taken (s): ",'{:.5f}'.format(timetaken))
    

